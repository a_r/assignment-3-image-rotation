#ifndef UTILS_H
#define UTILS_H
#include "image.h"
#pragma once
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_MALLOC,
    READ_ERROR
};
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};
void free_img(struct image* img);
uint16_t calculate_padding(uint32_t width);
#endif
