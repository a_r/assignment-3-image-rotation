#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate(struct image const* source, int angle) {
    struct image result;
    int isAngleOdd = ((angle + 360) % 180) != 0;
    result.width = isAngleOdd ? source->height : source->width;
    result.height = isAngleOdd ? source->width : source->height;
    result.data = calloc(result.width * result.height, sizeof(struct pixel));
    if (!result.data) {
        return (struct image){.width = 0, .height = 0, .data = NULL};
    }
    uint64_t src_width = source->width;
    uint64_t src_height = source->height;
    uint64_t src_size = src_width * src_height;

    for (uint64_t i = 0; i < src_size; ++i) {
        uint64_t x = i % src_width;
        uint64_t y = i / src_width;
        uint64_t dst_x, dst_y;

        if (angle == 90 || angle == -270) {
            dst_x = y;
            dst_y = src_width - x - 1;
        } else if (angle == 180 || angle == -180) {
            dst_x = src_width - x - 1;
            dst_y = src_height - y - 1;
        } else if (angle == 270 || angle == -90) {
            dst_x = src_height - y - 1;
            dst_y = x;
        } else {
            dst_x = x;
            dst_y = y;
        }

        uint64_t dst_index = dst_y * result.width + dst_x;
        result.data[dst_index] = source->data[i];
    }
    return result;
}
