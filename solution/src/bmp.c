#include "../include/bmp.h"
#include <stdlib.h>

#define BMP_FILE_TYPE 0x4D42
#define BMP_FILE_RESERVED 0
#define BMP_FILE_SIZE 40
#define BMP_FILE_PLANES 1
#define BMP_FILE_BITCOUNT 24
#define BMP_FILE_COMPRESSION 0
#define BMP_FILE_X_PELS_PER_METER 0
#define BMP_FILE_Y_PELS_PER_METER 0
#define BMP_FILE_CLR_USED 0
#define BMP_FILE_CLR_IMPORTANT 0

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    size_t res = fread(&header, sizeof(struct bmp_header), 1, in);
    if (res < 1) return READ_INVALID_HEADER;
    if (header.bfType != BMP_FILE_TYPE) { return READ_INVALID_SIGNATURE; }
    if (header.biBitCount != BMP_FILE_BITCOUNT) { return READ_INVALID_BITS; }
    img->width = header.biWidth;
    img->height = header.biHeight;

    uint16_t padding = calculate_padding(img->width);
    fseek(in, (long) header.bOffBits, SEEK_SET);
    img->data = (struct pixel*)malloc(img->width * img->height * sizeof(struct pixel));
    if (img->data == NULL) return READ_INVALID_MALLOC;

    for (uint64_t y = 0; y < img->height; y++) {
        res = fread(&img->data[y * img->width], sizeof(struct pixel), img->width, in);
        if (res < img->width) {
            free(img->data);
            return READ_ERROR;
        }
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = {
            .bfType = BMP_FILE_TYPE,
            .bfileSize = sizeof(struct bmp_header) + (img->width * sizeof(struct pixel) + ((4 - (img->width * sizeof(struct pixel)) % 4) % 4)) * img->height,
            .bfReserved = BMP_FILE_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_FILE_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_FILE_PLANES,
            .biBitCount = BMP_FILE_BITCOUNT,
            .biCompression = BMP_FILE_COMPRESSION,
            .biSizeImage = (img->width * sizeof(struct pixel) + ((4 - (img->width * sizeof(struct pixel)) % 4) % 4)) * img->height,
            .biXPelsPerMeter = BMP_FILE_X_PELS_PER_METER,
            .biYPelsPerMeter = BMP_FILE_Y_PELS_PER_METER,
            .biClrUsed = BMP_FILE_CLR_USED,
            .biClrImportant = BMP_FILE_CLR_IMPORTANT
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    uint16_t padding = calculate_padding(img->width);
    for (uint64_t y = 0; y < img->height; ++y) {
        if (fwrite(&img->data[y * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        fwrite((uint8_t*)"\0\0\0", sizeof(uint8_t), padding, out);
    }
    return WRITE_OK;
}
