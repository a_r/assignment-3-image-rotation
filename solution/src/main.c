#include "../include/bmp.h"
#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Invalid number of arguments\nUsage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }
    char* source_file = argv[1];
    char* transformed_file = argv[2];
    long angle = strtol(argv[3], NULL, 10);

    int valid_angles[] = {90, 180, 270, 0, -90, -180, -270};
    int num_valid_angles = sizeof(valid_angles) / sizeof(valid_angles[0]);

    int valid_angle = 0;
    for (int i = 0; i < num_valid_angles; ++i) {
        if (angle == valid_angles[i]) {
            valid_angle = 1;
            break;
        }
    }
    if (!valid_angle) {
        fprintf(stderr, "Invalid angle\n");
        return 1;
    }
    FILE* source_image_file = fopen(source_file, "rb");
    if (source_image_file == NULL) {
        fprintf(stderr, "Failed to open source image file\n");
        return 1;
    }
    struct image source_image;
    enum read_status read_result = from_bmp(source_image_file, &source_image);
    fclose(source_image_file);

    if (read_result != READ_OK) {
        fprintf(stderr, "Failed to read source image\n");
        free_img(&source_image);
        return 1;
    }
    struct image transformed_image = rotate(&source_image, (int)angle);
    free_img(&source_image);

    FILE* transformed_image_file = fopen(transformed_file, "wb");
    if (transformed_image_file == NULL) {
        fprintf(stderr, "Failed to open transformed image file\n");
        free_img(&transformed_image);
        return 1;
    }
    enum write_status write_result = to_bmp(transformed_image_file, &transformed_image);
    fclose(transformed_image_file);
    free_img(&transformed_image);

    if (write_result != WRITE_OK) {
        fprintf(stderr, "Failed to write transformed image\n");
        return 1;
    }
    return 0;
}
