#include "../include/image.h"
#include <stdlib.h>

void free_img(struct image *img) {
    free(img->data);
    img->data = NULL;
}
uint16_t calculate_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}
